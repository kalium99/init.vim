call plug#begin('~/.local/share/nvim/plugged')

Plug 'davidhalter/jedi-vim'
Plug 'neomake/neomake'

" Golang
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
" Plug 'zchee/deoplete-jedi'
Plug 'sbdchd/neoformat'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
" Plug 'jiangmiao/auto-pairs'
Plug 'sbdchd/neoformat'
Plug 'w0rp/ale'

" Solidity syntax
Plug 'tomlion/vim-solidity'

" JS Syntax
Plug 'pangloss/vim-javascript'

" Misc functionality
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}
Plug 'tpope/vim-obsession'
" Git
Plug 'tpope/vim-fugitive'
" Github Links
Plug 'knsh14/vim-github-link'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

" Octo

Plug 'pwntester/octo.nvim'

" Octo dependencies
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'kyazdani42/nvim-web-devicons'

call plug#end()

call neomake#configure#automake('nrwi', 500) 
let g:deoplete#enable_at_startup = 1


" FZF shortcut
nnoremap <C-p> :<C-u>FZF<CR> 


" Go Spaces and Tabs
au FileType go set noexpandtab
au FileType go set shiftwidth=4
au FileType go set softtabstop=4
au FileType go set tabstop=4
au FileType go set textwidth=130

" Go GoDef mappings
au FileType go nmap <Leader>ds <Plug>(go-def-split)
au FileType go nmap <Leader>dv <Plug>(go-def-vertical)
au FileType go nmap <Leader>dt <Plug>(go-def-tab)

" JS Spaces and Tabs
au FileType javascript set expandtab
au FileType javascript set shiftwidth=4
au FileType javascript set softtabstop=4
au FileType javascript set tabstop=4

" Python
autocmd FileType python setlocal tabstop=4 shiftwidth=4 smarttab expandtab

let g:go_highlight_build_constraints = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_operators = 1
let g:go_highlight_structs = 1
let g:go_highlight_types = 1
let g:go_auto_sameids = 1

let g:deoplete#enable_at_startup = 1
let g:neomake_python_enabled_makers = ['flake8']


" Error and warning signs.
let g:ale_sign_error = '⤫'
let g:ale_sign_warning = '⚠'

" Enable integration with airline.
let g:airline#extensions#ale#enabled = 1

" Look and Feel
let g:airline_theme='simple'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#branch#enabled=1

lua << EOF
require'nvim-web-devicons'.setup {
 -- your personnal icons can go here (to override)
 -- you can specify color or cterm_color instead of specifying both of them
 -- DevIcon will be appended to `name`
 -- globally enable default icons (default to false)
 -- will get overriden by `get_icons` option
 default = true;
}
EOF
